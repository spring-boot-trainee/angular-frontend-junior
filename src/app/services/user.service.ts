import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ILoginResponse } from '../interfaces/i-login-response';
import { IRegisterResponse } from '../interfaces/i-register-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = "http://localhost:8080";

  constructor(private http: HttpClient) {
    // this.baseUrl = environment.API_URL; // or using env
  }


  login(email: string, password: string): Observable<ILoginResponse> {
    let body = {
      email: email,
      password: password
    }
    return this.http.post<ILoginResponse>(this.baseUrl + "/user/login", body);
  }

  register(email: string, password: string, name: string): Observable<IRegisterResponse> {
    let body = {
      email: email,
      password: password,
      name: name
    }
    return this.http.post<IRegisterResponse>(this.baseUrl + "/user/register", body);
  }

  activateAccount(token: string): Observable<any> {
    let body = {
      token: token
    }
    return this.http.post<any>(this.baseUrl + "/user/activate", body);
  }

  resendActivationEmail(token: string) {
    let body = {
      token: token
    }
    return this.http.post<any>(this.baseUrl + "/user/resend-activation-email", body);
  }

}

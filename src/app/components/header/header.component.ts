import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppCookieService } from 'src/app/services/app-cookie.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private appCookieService: AppCookieService,
    private router: Router,
    public translateService: TranslateService
  ) { 
    this.translateService.addLangs(['en', 'th']);
    this.translateService.setDefaultLang('th');
  }

  ngOnInit(): void {
  }


  doLogout(): void {
    this.appCookieService.deleteAccessToken();
    this.router.navigate(['/login']);
  }

  translateSite(langauge: string = 'th') {
    this.translateService.use(langauge);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as Stomp from "stompjs"
import * as SockJS from "sockjs-client"
import { ChatService } from 'src/app/services/chat.service';
import { IChatMessage } from 'src/app/interfaces/i-chat-message';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  // about websockets
  private stompClient: any;
  public isConnected = false;
  private CHANNEL = "/topic/chat";
  private ENDPOINT = "http://localhost:8080/socket";

  messages: IChatMessage[] = [];

  // about form submit
  chatFormGroup: FormGroup = new FormGroup({
    message: new FormControl('', Validators.required)
  });

  constructor(private chatService: ChatService) { }

  ngOnInit(): void {
    this.connectWs()
  }

  private connectWs() {
    let ws = new SockJS(this.ENDPOINT)
    this.stompClient = Stomp.over(ws)

    let that = this;
    this.stompClient.connect({}, function () {
      console.log("websocket is Connected")
      that.isConnected = true;
      that.subscribeToGlobalChat()
    })
  }

  private subscribeToGlobalChat() {
    let that = this;
    this.stompClient.subscribe(this.CHANNEL, (message: any) => {
      let newMessage = JSON.parse(message.body) as IChatMessage;
      this.messages.push(newMessage);
    });
  }

  onSubmit() {
    console.log("onSubmit")
    let message = this.chatFormGroup.controls.message.value;

    // is connected?
    if (!this.isConnected) {
      alert('Please connect to Websocket');
      return;
    }

    console.log(message)
    this.chatService.postMessage(message).subscribe((response) => {
      console.log("response onSubmit", response)
    }, (error) => {
      console.error(error);
    })

  }

}

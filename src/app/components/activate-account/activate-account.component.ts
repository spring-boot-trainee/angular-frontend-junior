import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {

  private token: any;
  isSuccess: Boolean = false;
  errorMessage: String = ""
  errorCode = ""

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private router: Router, private translateService: TranslateService) { }

  ngOnInit(): void {

    this.token = this.activatedRoute.snapshot.paramMap.get('token');
    console.log(this.token)
    if (this.token === null) {
      // this.router.navigate(['/login']);
      return alert("don't activate");
    }

    this.activateAcc(this.token)
  }

  private activateAcc(token: string) {
    this.userService.activateAccount(token as string).subscribe(
      () => {
        // success
        this.isSuccess = true;
      }, (error) => {
        // on error
        console.error(error)
        let code = error.error.error;
        this.errorCode = code

        console.error("code", code)

        // @note จะใช้ได้ก็ต่อเมื่อมีการ set defaultLanguage: 'lang' ที่ app.module.ts  
        this.translateService.get("user." + code).subscribe((txt: string) => this.errorMessage = txt);

      }
    )

  }

  resendActivationEmail() {
    this.userService.resendActivationEmail(this.token as string).subscribe(
      () => {
        // success
        console.log("resendActivationEmail success")
      }, (error) => {
        // on error
        console.error(error);
      }
    );
  }

  navigateToLoginPage() {
    this.router.navigate(['/login']);
  }
}

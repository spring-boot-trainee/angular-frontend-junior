import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ILoginResponse } from 'src/app/interfaces/i-login-response';
import { AppCookieService } from 'src/app/services/app-cookie.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private userService: UserService,
    private appCookieService: AppCookieService,
    private router: Router
  ) { }

  loginFormGroup: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  ngOnInit(): void {
  }

  onSubmit(): void {
    if (this.loginFormGroup.invalid) {
      return;
    }
    console.log("on Submit", this.loginFormGroup.value)

    let email = this.loginFormGroup.controls.email.value;
    let password = this.loginFormGroup.controls.password.value;

    this.userService.login(email, password).subscribe((response: ILoginResponse) => {
      console.log(response)
      this.appCookieService.setAccessToken(response.token);
      this.router.navigate(['/dashboard']);
    }, (error) => {
      alert(error.error.error);
    });

  }

  onBtnRegister(): void {
    console.log("goto register")
    this.router.navigate(['/register']);
  }

}
